# 🟢 Status and Notices

---

The latest important ongoing notices, updates or announcements can be found here.

!!! Failure "Vanguard anticheat is coming to League of Legends, the era of League of Linux is over"
    :material-youtube: [Riot announcement video (Vanguard @ 12:12)](https://youtu.be/9U_jEzKf0_0?t=732)
    
    :material-reddit: [Farewell r/leagueoflinux: Vanguard is coming to League of Legends, likely ending the era of League of Linux](https://old.reddit.com/r/leagueoflinux/comments/1abhx9f/farewell_rleagueoflinux_vanguard_is_coming_to/)

    :material-reddit: [Collection of Rioter comments](https://old.reddit.com/r/leagueoflegends/comments/18zebss/what_do_you_guys_think_of_vangaurd/kgi2i6j/)

    :material-steam: [DotA2 Steam page](https://store.steampowered.com/app/570/Dota_2/)

!!! warning "Currently playable until Vanguard implementation"
    League of Legends is currently playable on Linux systems. See [the status page](status.md) for the latest information.

## ❓ Statuses

## :warning: THE BELOW IS NO LONGER MAINTAINED

### League of Legends

Feature | Functionality | Comments | Relevant Pages
----------|-----------------|----------------|---------------
Riot client | ✅ Full |  | 
LoL client | ✅ Full | Minor performance issues or visual bugs | [▶️ How to Install League of Legends](install/index.md), [📈 How to Optimise League of Legends](optimise/index.md)
LoL game | ❌ None | Currently crashing, see notices | [▶️ How to Install League of Legends](install/index.md), [📈 How to Optimise League of Legends](optimise/index.md)
LoL voice | ✅ Full | | 
LoL PBE server | ❌ Game crashing | Currently crashing, see notices | [▶️ How to Install League of Legends](install/index.md)
LoL China servers | ❓Unknown  | Please reach out to /u/TheAcenomad if you have reports of Chinese server performance | 

### Other Games

Game | Functionality | Comments | Relevant Pages
----------|-----------------|----------------|---------------
Teamfight Tactics | ✅ Full | Near native performance | [▶️ How to Install League of Legends](install/index.md), [🕹️ Other Riot Games on Linux](other_games/index.md)
VALORANT | ❌ None | Does not run native, via Wine, or via virtualsiation | [🕹️ Other Riot Games on Linux](other_games/index.md), [Vanguard Anticheat? What is it? Is it Coming to League?](faq/vanguard.md)
Legends of Runeterra |  ✅ Full |  | [▶️ How to Install League of Legends](install/index.md), [🕹️ Other Riot Games on Linux](other_games/index.md)
Ruined King: A League of Legends Story | ✅ Full | | [🕹️ Other Riot Games on Linux](other_games/index.md)
Hextech Mayhem: A League of Legends Story | ✅ Full | | [🕹️ Other Riot Games on Linux](other_games/index.md)
Song of Nunu: A League of Legends Story | ❓ Not yet released |  | [🕹️ Other Riot Games on Linux](other_games/index.md)
CONV/RGENCE: A League of Legends Story |  ❓ Not yet released |  | [🕹️ Other Riot Games on Linux](other_games/index.md)
Project L |  ❓ Not yet released |  | [🕹️ Other Riot Games on Linux](other_games/index.md)
The Mageseeker: A League of Legend Story | ✅ Full | | [🕹️ Other Riot Games on Linux](other_games/index.md)
Song of Nunu: A League of Legends Story | ❓ Not yet released |  | [🕹️ Other Riot Games on Linux](other_games/index.md)
