# Installing League of Legends via Snap

## What is Snap?

Snaps are a containerisation and packaging method developed by Canonical for Ubuntu and Ubuntu-based distributions. From the [about page](https://snapcraft.io/about):

>  Snaps are app packages for desktop, cloud and IoT that are easy to install, secure, cross‐platform and dependency‐free. Snaps are discoverable and installable from the Snap Store, the app store for Linux with an audience of millions.

## How to Install

1. Ensure you have `snap` installed. If you do not, then [follow the recommended installation](https://snapcraft.io/docs/installing-snapd) steps for your distribution
2. Install the [League of Legends \(WINE\) snap](https://snapcraft.io/leagueoflegends): `sudo snap install leagueoflegends --edge`

## Important Information

Although the initial installation is straightforward, the League of Legends snap can be difficult for non-technical users to configure or troubleshoot in the case of issues. Additionally, it tends to receive updates later than other methods. Consider using another installation method if you have issues with the snap package.

!!! info "Optimal performance settings"
    You may need to adjust some settings for optimal performance once installed. Read more in [📈 How to Optimise League of Legends](../optimise/index.md).

!!! failure "Something not working?"
    Read more on common problems, how to solve them, and other troubleshooting tips in [🛠️ Troubleshooting and Technical Support](../troubleshooting/index.md).