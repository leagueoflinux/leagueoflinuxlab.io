# 🗃️ Archive

From [humble beginnings](hhttps://web.archive.org/web/20230611125429/https://old.reddit.com/r/leagueoflinux/comments/jmo1m6/megathread_install_methods_anticheat_problemsbugs/), to the comprehensive (and significantly cleaner!) [leagueoflinux.org](https://leagueoflinux.org) today, there's a lot history in our online niche.

## [Previous Patch Megathreads](patch_threads.md)

A list of all the patch megathreads.

## Reddit Archive

TODO